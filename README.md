# Events and Tickets

Showcase how to use React with Material UI and Nestjs in the backend.

The app is fully functional. Of cause it can use lots of ux and ui polishing.

## Installation

This is a mono build and start the frontend and backend.

```sh
cd tickets-nest
npm i
npm run start:dev
cd ../tickets-react
npm i
npm run start
```

## Views

![views implemented](views-implemented.png)

## Technology involved

- mui-material
  - https://github.com/mui/material-ui/tree/v5.14.8/docs
  - MIT License
- useFetch with caching
  - https://github.com/juliencrn/usehooks-ts
  - MIT License
- useQuery with memory
  - https://v5.reactrouter.com/web/example/query-parameters
  - MIT License
- barcode generator
  - https://github.com/Bunlong/next-barcode
  - MIT License

## Todos

- Backend testing with jest
