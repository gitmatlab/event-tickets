export interface EventDTO {
  id: number;
  eventTitle: string;
  eventDate: string;
  eventCity?: string;
}

export interface TicketDTO {
  id: number;
  barcode: string;
  firstName: string;
  lastName: string;
  event?: { id: EventDTO["id"] };
}

export interface EventTicketsDTO extends EventDTO {
  tickets: TicketDTO[];
}
