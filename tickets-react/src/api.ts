interface State<T> {
  data?: T;
  error?: Error;
}

export async function post<T = unknown>(
  url: string,
  payload: { [key: string]: any }
): Promise<State<T>> {
  try {
    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: { "Content-Type": "application/json" },
    });
    if (!response.ok) {
      const message = await response.json();
      throw new Error(`${response.statusText} ${message}`);
      // throw new Error(response.statusText);
    }
    return { data: await response.json() };
  } catch (error: any) {
    console.log(`Connection error: ${error}`);
    return { error: error as Error };
  }
}
