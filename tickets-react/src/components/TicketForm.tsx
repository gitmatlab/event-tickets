import { Link } from "react-router-dom";
import { useState } from "react";
import { Box, Button, TextField } from "@mui/material";
import { post } from "../api";
import { TicketDTO } from "../types";
import Ticket from "./Ticket";
import { useQuery } from "../hooks/useQuery";
import Header from "./Header";

// TODO centralize url
const apiUrl = "http://localhost:3434";

export default function TicketForm() {
  const [view, setView] = useState<string>("form");
  const [response, setResponse] = useState<any>();

  const eventId = useQuery().get("eventId");
  if (!eventId) return <div>Ticket not found</div>;
  // TODO; handle no eventId smarter

  async function handleSubmit(event: React.ChangeEvent<HTMLFormElement>) {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const ticket = {
      ...Object.fromEntries(formData.entries()),
      event: { id: eventId },
    };
    const { data, error } = await post<TicketDTO>(`${apiUrl}/tickets`, ticket);
    if (error) {
      setView("error");
      setResponse(`error ${error}`);
    } else {
      setView("ticket");
      setResponse(data);
    }
  }

  return (
    <div>
      {view === "form" && (
        <ShowForm handleSubmit={handleSubmit} eventId={eventId} />
      )}
      {view === "error" && <div>{response}</div>}
      {view === "ticket" && <ShowTicket ticket={response} eventId={eventId} />}
    </div>
  );
}

function ShowForm({
  handleSubmit,
  eventId,
}: {
  handleSubmit: (event: React.ChangeEvent<HTMLFormElement>) => void;
  eventId: string;
}) {
  return (
    <div>
      <Header title="New Ticket" type="ticket" backLink={`/event/${eventId}`} />
      <Box
        component="form"
        onSubmit={handleSubmit}
        // noValidate
        sx={{ mt: 1 }}
      >
        <TextField
          margin="normal"
          required
          fullWidth
          id="firstName"
          label="First Name"
          name="firstName"
          autoComplete="firstName"
          autoFocus
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="lastName"
          label="Last Name"
          name="lastName"
          autoComplete="lastName"
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          Save
        </Button>
      </Box>
    </div>
  );
}

function ShowTicket({
  ticket,
  eventId,
}: {
  ticket: TicketDTO;
  eventId: string;
}) {
  return (
    <div>
      <Header
        title="Ticket is saved"
        type="ticket"
        backLink={`/event/${eventId}`}
      />
      <Ticket ticket={ticket} />
    </div>
  );
}
