import { useBarcode } from "next-barcode";

export default function Barcode({ code }: { code: string }) {
  const { inputRef } = useBarcode({
    value: code,
  });

  return <svg ref={inputRef} />;
}
