import { Card } from "@mui/material";
import { TicketDTO } from "../types";
import Barcode from "./Barcode";

export default function Ticket({ ticket }: { ticket: TicketDTO }) {
  return (
    <Card
      sx={{
        padding: "0.8rem",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Barcode code={ticket.barcode} />
      <div>
        {ticket.firstName} {ticket.lastName}
      </div>
    </Card>
  );
}
