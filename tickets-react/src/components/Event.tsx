import { Card } from "@mui/material";
import { EventDTO } from "../types";

export default function Event({ event }: { event: EventDTO }) {
  const { eventTitle, eventDate, eventCity } = event;
  return (
    <Card sx={{ padding: "20px" }}>
      <strong>{eventTitle}</strong>
      <div>
        {eventDate} {eventCity}
      </div>
    </Card>
  );
}
