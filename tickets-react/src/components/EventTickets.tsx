import { useParams } from "react-router-dom";
import { useFetch } from "../hooks/useFetch";
import { EventTicketsDTO, TicketDTO } from "../types";
import Ticket from "./Ticket";
import Event from "./Event";
import AddIcon from "@mui/icons-material/Add";
import { Fab, Typography } from "@mui/material";
import Header from "./Header";

// TODO centralize url
const apiUrl = "http://localhost:3434";

export default function EventTickets() {
  const { eventId } = useParams();
  const { data, error } = useFetch<EventTicketsDTO>(
    `${apiUrl}/events/${eventId}`
  );
  const event = data;
  const tickets = data?.tickets;

  if (error) {
    console.log(error);
    return (
      <div>
        Failed to load. Possibly a connection error. Is the server running?
      </div>
    );
  }

  if (!event) return <div>Event not found! EventId: {eventId}</div>;

  return (
    <div style={{ maxWidth: "500px", margin: "0 auto" }}>
      <div>
        <Header type="event" title="Event" backLink="/" />
        <Event event={event} />
        <Typography
          component="h1"
          variant="h5"
          textAlign="center"
          marginTop={"0.8rem"}
        >
          Tickets
        </Typography>
        {(!tickets || !tickets.length) && <div>No tickets</div>}
        {tickets &&
          tickets.map((ticket: TicketDTO) => (
            <Ticket key={ticket.id} ticket={ticket} />
          ))}
      </div>
      <Fab
        color="primary"
        aria-label="add"
        href={`/ticket/new?eventId=${eventId}`}
        sx={{
          position: "fixed",
          right: "4vh",
          bottom: "4vh",
        }}
      >
        <AddIcon />
      </Fab>
    </div>
  );
}
