import { AppBar, Avatar, IconButton, Toolbar, Typography } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import LocalActivityOutlinedIcon from "@mui/icons-material/LocalActivityTwoTone";
import TheaterComedyIcon from "@mui/icons-material/TheaterComedy";

export default function Header({
  backLink = "",
  type,
  title,
}: {
  backLink?: string;
  type: string;
  title: string;
}) {
  return (
    <div>
      <AppBar position="fixed">
        <Toolbar>{backLink && <NavBack backLink={backLink} />}</Toolbar>
      </AppBar>
      <HeaderTitle type={type} title={title} />
    </div>
  );
}

function NavBack({ backLink }: { backLink: string }) {
  return (
    <IconButton aria-label="back" href={backLink}>
      <ArrowBackIcon sx={{ color: "white" }} />
    </IconButton>
  );
}

function HeaderTitle({ type, title }: { type: string; title: string }) {
  return (
    <div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          {type === "event" && <TheaterComedyIcon />}
          {type === "ticket" && <LocalActivityOutlinedIcon />}
        </Avatar>
      </div>
      <Typography component="h1" variant="h5" textAlign="center">
        {title}
      </Typography>
    </div>
  );
}
