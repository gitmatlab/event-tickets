import { useFetch } from "../hooks/useFetch";
import { EventDTO } from "../types";
import { Fab, List, ListItemButton, ListItemText } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import Header from "./Header";

// TODO centralize url
const apiUrl = "http://localhost:3434";

export default function Events() {
  const { data, error } = useFetch<EventDTO[]>(`${apiUrl}/events`);
  if (error) return <div>Failed to load </div>;

  return (
    <div>
      <Header title="Events" type="event" />
      {(!data || !data.length) && <div>No events</div>}
      {data && EventList(data)}
    </div>
  );
}

function EventList(data: EventDTO[]) {
  return (
    <div>
      <List>
        {data &&
          data.map((event: EventDTO) => (
            <ListItemButton href={`/event/${event.id}`} key={event.id}>
              <ListItemText
                primary={event.eventTitle}
                secondary={`${event.eventDate} ${event.eventCity}`}
              ></ListItemText>
            </ListItemButton>
          ))}
      </List>

      <Fab
        color="primary"
        aria-label="add"
        href="/event/new"
        sx={{
          position: "fixed",
          right: "4vh",
          bottom: "4vh",
        }}
      >
        <AddIcon />
      </Fab>
    </div>
  );
}
