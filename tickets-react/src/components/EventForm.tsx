import { Link } from "react-router-dom";
import { useState } from "react";
import { post } from "../api";
import { EventDTO } from "../types";
import Header from "./Header";
import Event from "./Event";
import { Box, Button, TextField } from "@mui/material";

// TODO centralize url
const apiUrl = "http://localhost:3434";

export default function EventForm() {
  const [view, setView] = useState<string>("form");
  const [response, setResponse] = useState<any>();

  const handleSubmit = async (event: React.ChangeEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const eventData = Object.fromEntries(formData.entries());
    const { data, error } = await post<EventDTO>(`${apiUrl}/events`, eventData);
    if (error) {
      setView("error");
      setResponse(`error ${error}`);
    } else {
      setView("event");
      setResponse(data);
    }
  };

  return (
    <div>
      {view === "form" && <ShowForm handleSubmit={handleSubmit} />}
      {view === "error" && <div>{response}</div>}
      {view === "event" && <ShowEvent event={response} />}
    </div>
  );
}

function ShowForm({
  handleSubmit,
}: {
  handleSubmit: (event: React.ChangeEvent<HTMLFormElement>) => void;
}) {
  return (
    <div>
      <Header title="New Event" type="event" backLink="/" />
      <Box
        component="form"
        onSubmit={handleSubmit}
        // noValidate
        sx={{ mt: 1 }}
      >
        <TextField
          margin="normal"
          required
          fullWidth
          id="eventTitle"
          label="Event Title"
          name="eventTitle"
          autoComplete="eventTitle"
          autoFocus
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="eventDate"
          label="Date"
          name="eventDate"
          autoComplete="eventDate"
          type="date"
          InputLabelProps={{ shrink: true }}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="eventCity"
          label="City"
          name="eventCity"
          autoComplete="eventCity"
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          Save
        </Button>
      </Box>
    </div>
  );
}

function ShowEvent({ event }: { event: EventDTO }) {
  return (
    <div>
      <Header title="Event is saved" type="event" backLink="/" />
      <Event event={event}></Event>
    </div>
  );
}
