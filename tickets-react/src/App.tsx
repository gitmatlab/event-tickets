import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Events from "./components/Events";
import TicketForm from "./components/TicketForm";
import EventForm from "./components/EventForm";
import EventTickets from "./components/EventTickets";
import Layout from "./Layout";

// const apiUrl = "http://localhost:3434";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Events />,
  },
  {
    path: "/event/:eventId",
    element: <EventTickets />,
  },
  {
    path: "/ticket/new",
    element: <TicketForm />,
  },
  {
    path: "/event/new",
    element: <EventForm />,
  },
]);

export default function App() {
  return (
    <Layout>
      <RouterProvider router={router} />
    </Layout>
  );
}
