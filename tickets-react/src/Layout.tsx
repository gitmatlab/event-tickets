import { ThemeProvider } from "@emotion/react";
import {
  AppBar,
  Box,
  Container,
  CssBaseline,
  Toolbar,
  createTheme,
} from "@mui/material";
import { ReactElement } from "react";

export default function Layout({ children }: { children: ReactElement }) {
  return (
    <ThemeProvider theme={createTheme()}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          {children}
        </Box>
      </Container>
    </ThemeProvider>
  );
}
