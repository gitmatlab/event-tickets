/* Du hast vor dir 10 Maschinen, die Münzen prägen. Die Münzen sehen alle
 gleich aus! Die Münzen wiegen 100 Gramm. Allerdings ist eine Maschine kaputt
 und die Münzen der Maschine wiegen 10 Gramm. Münzen stehen unendlich pro
 Maschine bereit und es gibt eine Digitalwaage mit der ich einmal wiegen darf,
 um heraus zu finden welche Maschine defekt ist. Welche Maschine ist defekt? */

/* Lösung: Maschine 1 produziert 1 Münze, Maschine 2 produziert 2 Münzen etc.
 Wiege alle produzierten Münzen und errechne die Differenz zum erwarteten 
 Gewicht. Benutze diese Differenz und das Gewichtsverhältnis heile zu
 kaputte Münze um die Nummer der kaputten Maschine zu errechnen. */

// change parameters
const machineCount = 10;
const coinWeight = 100;
const coinWeightBroken = 10;

// use some crazy numbers
// const machineCount = 7;
// const coinWeight = 787;
// const coinWeightBroken = 29;


// no machine broken (brokenMachine = 0)
const optimalWeight = produceAndWeigh(0);
console.log("Optimal weight:", optimalWeight);

// test all machines
// output the machine number, that is broken
for (let brokenMachine = 0; brokenMachine <= machineCount; brokenMachine++) {
  evaluate(brokenMachine, optimalWeight);
}


// machine 1: 1 coin, machine 2: 2 coins etc.

function produceAndWeigh(brokenMachine) {
  let coinWeightSum = 0;
  for (let machine = 1; machine <= machineCount; machine++) {
    coinWeightSum +=
      machine !== brokenMachine
        ? machine * coinWeight
        : machine * coinWeightBroken;
  }
  return coinWeightSum;
}

function evaluate(brokenMachine, expectedWeightAllCoins) {
  // test
  const actualWeightAllCoins = produceAndWeigh(brokenMachine);

  console.log(" ");
  console.log("Broken machine:", brokenMachine);
  console.log("Weight:", actualWeightAllCoins);
  console.log("Diff:", expectedWeightAllCoins - actualWeightAllCoins);
  // console.log("Sum:", expectedWeightAllCoins + actualWeightAllCoins);

  // evaluate
  const diffOptimalActual = expectedWeightAllCoins - actualWeightAllCoins   // e.g. 180
  const diffOptimalBroken = coinWeight - coinWeightBroken                   // 100 - 10 = 90
  const quoteOptimalBroken = coinWeight / diffOptimalBroken                 // 100 / 90
  let machineNumber = (diffOptimalActual * quoteOptimalBroken) / coinWeight // 180 * 100/90 / 100 = 2

  actualWeightAllCoins === expectedWeightAllCoins
    ? console.log("No machine is broken")
    : console.log("Evaluated machine #:", machineNumber);
  
  console.log("Evalutation is: ", brokenMachine === machineNumber);
}

