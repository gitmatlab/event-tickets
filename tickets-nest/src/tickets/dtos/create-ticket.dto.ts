import { IsNotEmpty, IsObject, IsString } from 'class-validator';
import { Event } from '../../events/event.entity';

export class CreateTicketDto {
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsObject()
  event: Event;
}
