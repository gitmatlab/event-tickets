import { Test, TestingModule } from '@nestjs/testing';
import { TicketsService } from './tickets.service';
import { Ticket } from './ticket.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Event } from '../events/event.entity';
import {
  TypeOrmModuleTesting,
  getEntityManager,
} from '../../test/TypeOrmModuleTesting';

describe('TicketsService', () => {
  let service: TicketsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModuleTesting([Ticket, Event]),
        TypeOrmModule.forFeature([Ticket, Event]),
      ],
      providers: [TicketsService],
    }).compile();

    service = module.get<TicketsService>(TicketsService);
  });

  beforeAll(async () => {
    // seed database
    const entityManager = await getEntityManager([Ticket, Event]);
    await entityManager.clear(Ticket);
    await entityManager.clear(Event);
    await entityManager.insert(Event, {
      id: 1,
      eventTitle: 'Taylor Swift',
      eventDate: '2023-10-01',
      eventCity: 'Köln',
    });
    await entityManager.insert(Ticket, {
      id: 1,
      firstName: 'Erika',
      lastName: 'Mustermann',
      barcode: '12345678',
      event: <Event>{ id: 1 },
    });
    // silence console output
    jest.spyOn(console, 'log').mockImplementation(() => {});
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('methods', () => {
    it('lists all tickets', async () => {
      const result = await service.find();
      expect(Array.isArray(result)).toBeTruthy();
      // initally one ticket is in the db.
      // There might be more, since the tests run in parallel
      expect(result.length).toBeGreaterThanOrEqual(1);
    });

    it('creates a ticket', async () => {
      const result = await service.create('Max', 'Mustermann', <Event>{
        id: 1,
      });
      expect(result.barcode).toHaveLength(8);
    });

    it('findOne finds a ticket by id', async () => {
      const result = await service.findeOne(1);
      expect(result).toBeInstanceOf(Ticket);
      expect(result.barcode).toHaveLength(8);
    });

    it('findOne throws an error if ticket id does not exist', async () => {
      expect.assertions(1);
      try {
        await service.findeOne(0);
      } catch (e) {
        // console.log(e.response.error);
        // NotFoundException standard message
        expect(e.response.error).toMatch('Not Found');
      }
    });
    it('updates a ticket', async () => {
      const result = await service.update(1, {
        firstName: 'Jolly',
        lastName: 'Joker',
      });
      expect(result.firstName).toMatch('Jolly');
      expect(result.lastName).toMatch('Joker');
    });
  });
});
