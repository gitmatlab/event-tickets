import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { TicketsService } from './tickets.service';
import { CreateTicketDto } from './dtos/create-ticket.dto';
import { UpdateTicketDto } from './dtos/update-ticket.dto';

@Controller('tickets')
export class TicketsController {
  constructor(private ticketService: TicketsService) { }

  @Post()
  createTicket(@Body() body: CreateTicketDto) {
    return this.ticketService.create(body.firstName, body.lastName, body.event);
  }

  @Get()
  findAllTickets() {
    return this.ticketService.find();
  }

  @Get('/:id')
  findTicket(@Param('id') id: string) {
    return this.ticketService.findeOne(parseInt(id));
  }

  @Patch('/:id')
  updateTicket(@Param('id') id: string, @Body() body: UpdateTicketDto) {
    return this.ticketService.update(parseInt(id), body);
  }

  @Delete('/:id')
  removeTicket(@Param('id') id: string) {
    return this.ticketService.remove(parseInt(id));
  }
}
