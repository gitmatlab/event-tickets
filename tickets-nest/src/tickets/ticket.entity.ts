import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Event } from '../events/event.entity';

@Entity()
export class Ticket {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  barcode: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @ManyToOne(() => Event, (event) => event.tickets)
  event: Event;
}
