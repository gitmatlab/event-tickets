import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Ticket } from './ticket.entity';
import { Event } from '../events/event.entity';

@Injectable()
export class TicketsService {
  static barcodeNumbers: number = 8;

  constructor(@InjectRepository(Ticket) private repo: Repository<Ticket>) { }

  async create(firstName: string, lastName: string, event: Event) {
    const barcode = TicketsService.generateBarcode(
      TicketsService.barcodeNumbers,
    );
    try {
      const ticket = this.repo.create({ barcode, firstName, lastName, event });
      return await this.repo.save(ticket);
    } catch (e) {
      throw new BadRequestException(`Event with id ${event.id} not found`);
    }

  }

  find() {
    return this.repo.find();
  }

  async findeOne(id: number) {
    const ticket = await this.repo.findOneBy({ id });
    if (!ticket) throw new NotFoundException(`Ticket not found ${id}`);
    return ticket;
  }

  async update(id: number, attrs: Partial<Ticket>) {
    const ticket = await this.repo.findOneBy({ id });
    if (!ticket) throw new NotFoundException(`Ticket not found ${id}`);
    Object.assign(ticket, attrs);
    return this.repo.save(ticket);
  }

  async remove(id: number) {
    const ticket = await this.repo.findOneBy({ id });
    if (!ticket) throw new NotFoundException(`Ticket not found ${id}`);
    return this.repo.remove(ticket);
  }

  static generateBarcode = (digits: number) => {
    const min = 10 ** (digits - 1);
    const max = 10 ** digits;
    const randomNumber = Math.floor(min + Math.random() * (max - min));
    return randomNumber.toString();
  };
}
