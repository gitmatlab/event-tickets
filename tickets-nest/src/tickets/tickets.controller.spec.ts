import { Test, TestingModule } from '@nestjs/testing';
import { TicketsController } from './tickets.controller';
import { TicketsService } from './tickets.service';
import { Ticket } from './ticket.entity';

// Only two methods are tested. There is not much value in testing as the
// methods don't have any logic and return only the mocked service data.
// The only interesting part is the path and query match, that can't be
// tested. Same is true to the validation pipe. Both will be covered in
// the e2e tests.
describe('TicketController', () => {
  let controller: TicketsController;
  let fakeTicketService: Partial<TicketsService>;

  beforeEach(async () => {
    fakeTicketService = {
      create: (firstName, lastName, event) =>
        Promise.resolve(<Ticket>{
          id: 1,
          firstName,
          lastName,
          event,
          barcode: '12345678',
        }),
      find: () => Promise.resolve(<Ticket[]>[]),
      findeOne: (id) =>
        Promise.resolve(<Ticket>{
          id,
          firstName: 'Max',
          lastName: 'Mustermann',
          event: { id: 1 },
          barcode: '12345678',
        }),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [TicketsController],
      providers: [
        {
          provide: TicketsService,
          useValue: fakeTicketService,
        },
      ],
    }).compile();

    controller = module.get<TicketsController>(TicketsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('finds tickets', async () => {
    const result = await controller.findAllTickets();
    expect(Array.isArray(result)).toBeTruthy();
    expect(result.length).toBe(0);
  });

  it('findTicket by id', async () => {
    const result = await controller.findTicket('1');
    expect(result.firstName).toBe('Max');
  });
});
