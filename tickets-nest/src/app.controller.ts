import { Controller, Get } from '@nestjs/common';

@Controller('/')
export class AppController {
  @Get('/health')
  checkHealth() {
    console.log('OK');
    return 'OK';
  }
}
