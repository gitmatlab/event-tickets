import { Module, ValidationPipe } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { Ticket } from './tickets/ticket.entity';
import { TicketsModule } from './tickets/tickets.module';
import { EventsModule } from './events/events.module';
import { Event } from './events/event.entity';

// the ConfigModule seems to be overdone as 
//  env files are so easy to use
const isTest = process.env.NODE_ENV === 'test';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: isTest ? 'data/test.sqlite' : 'data/db.sqlite',
      entities: [Ticket, Event],
      synchronize: true,
    }),
    TicketsModule,
    EventsModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: 'APP_PIPE',
      useValue: new ValidationPipe({
        whitelist: true,
      }),
    },
  ],
})
export class AppModule { }
