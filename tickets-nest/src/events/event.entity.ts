import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Ticket } from '../tickets/ticket.entity';

@Entity()
export class Event {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  eventTitle: string;

  @Column()
  eventDate: string;

  @Column()
  eventCity: string;

  @OneToMany(() => Ticket, (ticket) => ticket.event, { eager: true })
  tickets: Ticket[];
}
