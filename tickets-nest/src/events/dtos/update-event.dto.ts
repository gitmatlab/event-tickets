import { IsDateString, IsString, IsOptional } from 'class-validator';

export class UpdateEventDto {
  @IsString()
  @IsOptional()
  eventTitle: string;

  @IsDateString()
  @IsOptional()
  eventDate: string;

  @IsString()
  @IsOptional()
  eventCity: string;
}
