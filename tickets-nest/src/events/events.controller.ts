import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { CreateEventDto } from './dtos/create-event.dto';
import { EventsService } from './events.service';
import { UpdateEventDto } from './dtos/update-event.dto';

@Controller('events')
export class EventsController {
  constructor(private eventService: EventsService) { }

  @Post()
  createEvent(@Body() body: CreateEventDto) {
    return this.eventService.create(
      body.eventTitle,
      body.eventDate,
      body.eventCity,
    );
  }

  @Get()
  findAllEvents() {
    return this.eventService.find();
  }

  @Get('/:id')
  findEvent(@Param('id') id: string) {
    return this.eventService.findeOne(parseInt(id));
  }

  @Patch('/:id')
  updateEvent(@Param('id') id: string, @Body() body: UpdateEventDto) {
    return this.eventService.update(parseInt(id), body);
  }

  @Delete('/:id')
  removeEvent(@Param('id') id: string) {
    return this.eventService.remove(parseInt(id));
  }
}
