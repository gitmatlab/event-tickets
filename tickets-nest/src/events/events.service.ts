import { Injectable, NotFoundException } from '@nestjs/common';
import { Event } from './event.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class EventsService {
  constructor(@InjectRepository(Event) private repo: Repository<Event>) {}

  create(eventTitle: string, eventDate: string, eventCity: string) {
    const event = this.repo.create({ eventTitle, eventDate, eventCity });
    return this.repo.save(event);
  }

  find() {
    return this.repo.find();
  }

  async findeOne(id: number) {
    const event = await this.repo.findOneBy({ id });
    if (!event) throw new NotFoundException(`Event not found ${id}`);
    return event;
  }

  async update(id: number, attrs: Partial<Event>) {
    const event = await this.repo.findOneBy({ id });
    if (!event) throw new NotFoundException(`Event not found ${id}`);
    Object.assign(event, attrs);
    return this.repo.save(event);
  }

  async remove(id: number) {
    const event = await this.repo.findOneBy({ id });
    if (!event) throw new NotFoundException(`Event not found ${id}`);
    return this.repo.remove(event);
  }
}
