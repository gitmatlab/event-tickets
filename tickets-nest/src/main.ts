import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 3434;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
  await app.listen(port, host, () => console.log('Listening on:', host, port));
}
bootstrap();
