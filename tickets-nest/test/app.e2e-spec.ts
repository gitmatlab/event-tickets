import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('Controller common (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/health (GET)', () => {
    return request(app.getHttpServer()).get('/health').expect(200).expect('OK');
  });


  it('/tickets (GET) all tickets', () => {
    return request(app.getHttpServer())
      .get('/tickets')
      .expect(200)
      .then((res) => {
        expect(Array.isArray(res.body)).toBeTruthy();
      });
  });

  it('/tickets (POST) create ticket', () => {
    return request(app.getHttpServer())
      .post('/tickets')
      .send({ firstName: 'Max', lastName: 'Mustermann', event: { id: 1 } })
      .expect(201)
      .then((res) => {
        expect(res.body.id).toBeGreaterThan(0)
      });
  })

  it('/tickets (POST) create ticket with invalid event', () => {
    return request(app.getHttpServer())
      .post('/tickets')
      .send({ firstName: 'Max', lastName: 'Mustermann', event: { id: 999 } })
      .expect(400)
      .then((res) => {
        expect(res.body.message).toBe('Event with id 999 not found')
      });
  })

  it('/tickets (POST) create ticket with empty firstName', () => {
    return request(app.getHttpServer())
      .post('/tickets')
      .send({ firstName: '', lastName: 'Mustermann', event: { id: 1 } })
      .expect(400)
  })

  it('/tickets (POST) create ticket with empty lastName', () => {
    return request(app.getHttpServer())
      .post('/tickets')
      .send({ firstName: 'Max', lastName: '', event: { id: 1 } })
      .expect(400)
  })

  it('/tickets/:id (GET) find ticket', () => {
    return request(app.getHttpServer())
      .get('/tickets/1')
      .expect(200)
      .then((res) => {
        expect(res.body.id).toBe(1)
      });
  })

  it('/tickets/:id (PATCH) update ticket', () => {
    return request(app.getHttpServer())
      .patch('/tickets/1')
      .send({ firstName: 'Micha' })
      .expect(200)
      .then((res) => {
        expect(res.body.firstName).toBe('Micha')
      });
  })

  // potential conflicting with parallel tests (e.g. PATCH)
  // that expect id 1 to exist
  xit('/tickets/:id (DELETE) remove ticket', () => {
    return request(app.getHttpServer())
      .delete('/tickets/1')
      .expect(200)
      .then((res) => {
        expect(res.body.id).toBe(1)
      });
  })

  it('/events (GET) find all events', () => {
    return request(app.getHttpServer())
      .get('/events')
      .expect(200)
      .then((res) => {
        expect(Array.isArray(res.body)).toBeTruthy();
      });
  })

  it('/events (POST) create event', () => {
    return request(app.getHttpServer())
      .post('/events')
      .send({ eventTitle: 'Madonna Recoated', eventDate: '2024-01-01', eventCity: 'Leibzig' })
      .expect(201)
      .then((res) => {
        expect(res.body.id).toBeGreaterThan(0)
      });
  })

  it('/events (POST) create event with empty eventTitle', () => {
    return request(app.getHttpServer())
      .post('/events')
      .send({ eventTitle: '', eventDate: '2024-01-01', eventCity: 'Leibzig' })
      .expect(400)
  })

  it('/events (POST) create event with empty eventDate', () => {
    return request(app.getHttpServer())
      .post('/events')
      .send({ eventTitle: 'Madonna Recoated', eventDate: '', eventCity: 'Leibzig' })
      .expect(400)
  })

  it('/events (POST) create event with invalid eventDate', () => {
    return request(app.getHttpServer())
      .post('/events')
      .send({ eventTitle: 'Madonna Recoated', eventDate: '123456789', eventCity: 'Leibzig' })
      .expect(400)
  })

  it('/events (POST) create event with empty eventCity', () => {
    return request(app.getHttpServer())
      .post('/events')
      .send({ eventTitle: 'Madonna Recoated', eventDate: '2024-01-01', eventCity: '' })
      .expect(400)
  })

  it('/events/:id (GET) find event', () => {
    return request(app.getHttpServer())
      .get('/events/1')
      .expect(200)
      .then((res) => {
        expect(res.body.id).toBe(1)
      });
  })

  it('/events/:id (PATCH) update event', () => {
    return request(app.getHttpServer())
      .patch('/events/1')
      .send({ eventTitle: 'Madonna Recoated 2' })
      .expect(200)
      .then((res) => {
        expect(res.body.eventTitle).toBe('Madonna Recoated 2')
      });
  })

  // 
  xit('/events/:id (DELETE) remove event', () => {
    return request(app.getHttpServer())
      .delete('/events/1')
      .expect(200)
      .then((res) => {
        expect(res.body.id).toBe(1)
      });
  })

});
