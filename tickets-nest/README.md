# Tickets

Nestjs backend.
Uses Typeorm for creating the database.

## Testing

### e2e Testing

The e2e tests are complete. As these tests can also cover missing parameters (validation pipe)

In this app *all* e2e tests are in a single file in `/test/app.e2e-spec.ts`.

Funny that the e2e tests don't appear in the test coverage report.

### Unit testing controller

Unit testing of controller proved to be not valuable. The methods don't have any logic and return only the mocked service data. The only interesting part is the path and query match, which can't be tested in this kind of unit testing. The same is true for the validation pipe. Both will be covered in the e2e tests.

### Unit testing service

The unit tests of the services seem to be the most important. But they overlab with the e2e tests so much, that it seems more efficient to focus on the e2e tests. This is only true, as long all execptions in the service are thrown up to the controller. What is the case with this application.

The unit test of the ticket service uses a test database. The test database and the data seed can be found in `/utils/TypeOrmModuleTesting.ts`.